﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace cwiczenie2
{
    class Program
    {
        // Napisać program konsolowy, który będzie obliczać kolejne potęgi liczby 2. Użyć pętli for.
        // Wersja rozszerzona Zabezpieczyć przed niepoprawnymi wartościami podawanymi przez użytkownika:
        // wartości wykładnika mniejsze od zera
        // wpisanie czegokolwiek innego niż liczba(instrukcja try..catch..).
        static void Main(string[] args)
        {
            try
            {
                Console.WriteLine("Podaj dodatni wykładnik");
                int wykladnik = int.Parse(Console.ReadLine());
                if (wykladnik > 0)
                {
                    int potega = 1;
                    for (int i = 1; i <= wykladnik; i++)
                    {
                        potega = potega * 2;
                        Console.WriteLine("2 do {0} = {1}", i, potega);
                    }
                }
                else
                {
                    Console.WriteLine("Wykładnik jest mniejszy od zera");
                }
            }
            catch(Exception ex)
            {
                Console.WriteLine("Coś poszło nie tak :(... Spróbuj uruchomić program jeszcze raz");
            }
            
            Console.ReadKey();
        }
    }
}
